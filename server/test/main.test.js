const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect, use } = require('chai');
const { describe, it } = require('mocha');

const app = require('../src/index');

use(chaiHttp);

describe('Test API', () => {
    it('Should return message Hello Test', () => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.a('object');
                expect(res.body).to.have.property('message')
                    .and.to.be.eql("Hello Test");
            });
    });
});
