const router = require('express').Router();
const userCtrl = require('../controllers/userCtrl');

router.post('/login', userCtrl.login);
router.get('/logout', userCtrl.logout);
router.post('/register', userCtrl.register);
router.get('/refreshtoken', userCtrl.refreshToken);

module.exports = router;
