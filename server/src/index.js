const express = require('express');
const bodeParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');

const config = require('./config');

const app = express();

dotenv.config();

app.use(bodeParser.json({ limit: "30mb", extend: true }));
app.use(bodeParser.urlencoded({ limit: "30mb", extend: true }));
app.use(cors());

// Routes
app.get('/', (req, res) => res.status(200).json({ message: 'Hello Test' }));
app.use('/auth', require('./routes/userRouter'));
app.use('/api', require('./routes/categoryCtrl'));
app.use('/api', require('./routes/productRouter'));

mongoose.connect('mongodb+srv://dimalosev:demigod111@cluster0.mxmfn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(config.port, () => console.log(`Server running on port: ${config.port}`)))
    .catch((error) => console.log(error.message));

mongoose.set('useFindAndModify', false);

module.exports = app;
