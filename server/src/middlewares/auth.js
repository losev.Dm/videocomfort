const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    try {
        const token = req.header('Authorization');

        if (!token) return res.status(400).json({ message: "Invalid Authorization." });

        jwt.verify(token, process.env.ACCESS_TOKEN_KEY, (err, user) => {
            if (err) return res.status(400).json({ message: 'Invalid Authorization.' });

            next();
        });
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
};

module.exports = authMiddleware;
