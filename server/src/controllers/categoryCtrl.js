const Categories = require('../models/categoryModel');

const categoriesCtrl = {
    getCategories: async (req, res) => {
        try {
            const categories = await Categories.find();

            res.status(200).json(categories);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
};

module.exports = categoriesCtrl;
