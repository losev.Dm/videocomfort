const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');

const userCtrl = {
    login: async (req, res) => {
        try {
            const { email, password } = req.body;

            const user = await User.findOne(email);
            if (!user) return res.status(400).json({ message: 'User does not exist.'});

            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) return res.status(401).json({ message: 'Invalid password.' });

            const accessToken = createAccessToken({ id: user._id });
            const refreshToken = createRefreshToken({ id: user._id });

            res.cookie('refreshtoken', refreshToken, {
                httpOnly: true,
                path: '/auth/refresh_token'
            });
            res.status(200).json(accessToken);
        } catch (e) {
            res.status(500).json({ message: e.message })
        }
    },
    logout: async (req, res) => {
        try {
            res.clearCookie('refreshtoken', { path: '/auth/refresh_token' });
            return res.status(200).json({ message: 'Logged out success.'})
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    },
    register: async (req, res) => {
        try {
            const { name, email, password } = req.body;

            const user = await User.findOne({ email });

            if (user) return res.status(400).json({ message: 'The user already existing.'});
            if (password.length < 6) return res.status(400).json({ message: 'Password is at least 6 characters long.' });

            const passwordHash = await bcrypt.hash(password, 10);
            const newUser = new User({ name, email, password: passwordHash });
            const accessToken = createAccessToken({ id: newUser._id });
            const refreshToken = createRefreshToken({ id: newUser._id });

            await newUser.save();

            res.cookie('refreshtoken', refreshToken, {
                httpOnly: true,
                path: '/auth/refresh_token'
            });
            res.status(200).json(accessToken);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    },
    refreshToken: (req, res) => {
        try {
            const refreshToken = req.cookie.refreshtoken;

            if(!refreshToken) return res.status(400).json({ message: 'You need login or register.' });

            jwt.verify(refreshToken, process.env.REFRESH_TOKEN_KEY, (err, user) => {
                if (err) return res.status(400).json({ message: 'You need login or register.' });

                const accessToken = createAccessToken({ id: user._id });

                res.json({ accessToken });
            });
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
};

const createAccessToken = (user) => {
  return jwt.sign(user, process.env.ACCESS_TOKEN_KEY, { expiresIn: '1h' });
};

const createRefreshToken = (user) => {
  return jwt.sign(user, process.env.REFRESH_TOKEN_KEY, { expiresIn: '1d' });
};

module.exports = userCtrl;
