const Products = require('../models/productModel');

const productCtrl = {
    getProducts: async (req, res) => {
        try {
            const products = await Products.find();

            res.status(200).json(products);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
};

module.exports = productCtrl;
